import numpy as np
import matplotlib.pyplot as plt

learning_rate = 0.01

test_data = [
    ([4, 2.0, 150, 220, 75, 17, 5, 1.4, 1, -1], 0),  # auto
    ([4, 5.0, 450, 270, 500, 19, 5, 1.6, 1, -1], 0),  # szybkie auto
    ([4, 4.0, 200, 270, 500, 19, 5, 1.6, 1, -1], 0),  # szybkie auto
    ([4, 1.9, 120, 180, 68, 16, 5, 1.5, 1, 1], 0),  # auto
    ([4, 1.2, 75, 165, 46, 14, 4, 1.2, 1, -1], 0),   # auto
    ([2, 0.7, 90, 220, 28, 16, 2, 0.5, -1, -1], 1),    # motor
    ([2, 0.5, 80, 250, 38, 16, 2, 0.4, -1, -1], 1),    # motor
    ([2, 1.5, 120, 300, 78, 18, 1, 0.6, -1, -1], 1),    # motor
    ([2, 1.2, 170, 330, 100, 18, 1, 0.8, -1, -1], 1),    # motor
    ([6, 10, 220, 120, 500, 22, 3, 9, 1, 1], 0),  # tir
    ([4, 2.0, 150, 50, 300, 45, 1, 4, 1, 1], 0),   # traktor
    ([4, 2.5, 170, 60, 350, 40, 2, 5, 1, 1], 0),  # traktor
    ([4, 3.0, 140, 120, 200, 25, 1, 3, 1, 1], 0),   # ciezarowka
    ([4, 2.0, 160, 120, 24, 22, 1, 3, 1, 1], 0)   # ciezarowka
]

max_values = np.array([6, 5, 500, 400, 500, 20, 10, 10, 1, 1])

def sigmoidal(u):
    return 1 / (1 + np.exp(-1 * u))


def normalize(u):
    return u / max_values


def run_network(weights, args):
    u = weights.dot(args.T)
    return sigmoidal(u)


def teach_network(data, epochs):
    weights = np.random.rand(1, 10)
    data_size = len(data)

    sum_err_data = []
    for ep in range(0, epochs):
        sum_err = 0.0
        for i in range(0, data_size):
            index = np.random.randint(0, data_size)
            input_arg = normalize(np.array(data[index][0]))
            expected = data[index][1]

            predicted = run_network(weights, input_arg)
            error = expected - predicted
            sum_err += error ** 2
            weights += learning_rate * input_arg * error
        print(str(ep) + " epoch " + str(sum_err))
        sum_err_data.append(float(sum_err))

    plt.plot(sum_err_data)

    return weights


print("========== go go perceptrony =============")

neuron_weights = teach_network(test_data, 100)

print("=========== wagi sieci: ==============")
print(neuron_weights)

new_example = normalize(np.array([4, 3.2, 180, 210, 20, 20, 5, 1.8, 1, 1]))  # auto terenowe

result = run_network(neuron_weights, new_example)
print("========== wynik testu: ==============")
print(str(result))

plt.show()
